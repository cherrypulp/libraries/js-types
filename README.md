# Types

[![codebeat badge](https://codebeat.co/badges/0f7f02a2-71e4-4bb1-bf00-b8d62bb810b6)](https://codebeat.co/projects/gitlab-com-cherrypulp-libraries-js-types-master)
[![Issues](https://img.shields.io/badge/issues-0-brightgreen)](https://gitlab.com/cherrypulp/libraries/js-types/issues)
[![License](https://img.shields.io/npm/l/@cherrypulp/types)](https://gitlab.com/cherrypulp/libraries/js-types/blob/master/LICENSE)
[![npm version](https://badge.fury.io/js/%40cherrypulp%2Ftypes.svg)](https://badge.fury.io/js/%40cherrypulp%2Ftypes)

Type check library.


## Installation

`npm install @cherrypulp/types`


## Quick start

```javascript
import * as types from '@cherrypulp/types';

// or

import {isBoolean/*, ...*/} from '@cherrypulp/types';
```

### Methods

#### `isArray`

Return true if value is an array.

```javascript
isArray([]); // return true
isArray(null); // return false
```

#### `isBoolean`

Return true if value is a boolean.

```javascript
isBoolean(false); // return true
isBoolean('true'); // return false
```

#### `isCallable`

Return true if value is a callable object or function.

```javascript
isCallable(function () {/**/}); // return true
isCallable(null); // return false
```

#### `isDate`

Return true if value is date object.

```javascript
isDate(new Date()); // return true
isDate({}); // return false
```

#### `isDataURL`

Return true if value is a data URL.

```javascript
isDataURL('data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7'); // return true
isDataURL('foo'); // return false
```

#### `isEmail`

Return true if value is an email.

```javascript
isEmail('john@doe.com'); // return true
isEmail('foo'); // return false
```

#### `isEmpty`

Return true if value is empty ('', {}, [], null or undefined).

```javascript
isEmpty(''); // return true
isEmpty({}); // return true
isEmpty([]); // return true
isEmpty(null); // return true
isEmpty(undefined); // return true
isEmpty('foo'); // return false
isEmpty({foo: 'bar'}); // return false
isEmpty(['foo']); // return false
````

#### `isError`

Return true if value is an Error object.

```javascript
isError(new Error()); // return true
isError({}); // return false
```

#### `isFalse`

Return true if value is false.

```javascript
isFalse(false); // return true
isFalse('false'); // return false
```

#### `isFloat`

Return true if value is a float number.

```javascript
isFloat(false); // return true
isFloat('false'); // return false
```

#### `isFunction`

Return true if value is a function.

```javascript
isFunction(function () {/**/}); // return true
isFunction(class {}); // return true
isFunction(null); // return false
```

#### `isInteger`

Return true if value is integer.

```javascript
isInteger(1); // return true
isInteger(1.4); // return false
```

#### `isNull`

Return true if value is null.

```javascript
isNull(null); // return true
isNull(''); // return false
```

#### `isNumeric`

Return true if value is numeric.

```javascript
isNumeric(1); // return true
isNumeric(1.4); // return true
isNumeric('3'); // return false
```

#### `isObject`

Return true if value is an object.

```javascript
isObject({}); // return true
isObject(new Date()); // return false
```

#### `isPromise`

Return true if value is a promise.

```javascript
isPromise(new Promise((resolve) => resolve())); // return true
isPromise(function () {/**/}); // return false
```

#### `isRegExp`

Return true if value is a RegExp.

```javascript
isRegExp(/.*/); // return true
isRegExp(new RegExp('.*')); // return true
isRegExp('.*'); // return false
```

#### `isString`

Return true if value is a string.

```javascript
isString('foo'); // return true
isString(3); // return false
```

#### `isSymbol`

Return true if value is a symbol.

```javascript
isSymbol(Symbol('foo')); // return true
isSymbol('foo'); // return false
```

#### `isTrue`

Return true if value is true.

```javascript
isTrue(true); // return true
isTrue('true'); // return false
```

#### `isTypeOf`

Return true if value typeof is equal to given type.

```javascript
isTypeOf({}, 'object'); // return true
isTypeOf(null, 'string'); // return false
```

#### `isUndefined`

Return true if value is undefined.

```javascript
isUndefined(undefined); // return true
isUndefined(null); // return false
```

## Versioning

Versioned using [SemVer](http://semver.org/).

## Contribution

Please raise an issue if you find any. Pull requests are welcome!

## Author

  + **Stéphan Zych** - [monkeymonk](https://gitlab.com/monkey_monk)

## License

This project is licensed under the MIT License - see the [LICENSE](https://gitlab.com/cherrypulp/libraries/js-types/blob/master/LICENSE) file for details.



## TODO

- [ ] add `isAffirmative` (ex. true, 'true', '1', 1, 'yes', 'y', or 'ok') and `isNegative` (ex. false, 'false', '0', 0, 'no', 'n', or 'cancel')
- [ ] add `isURL`
- [ ] add `isCreditCard`
- [ ] add `isColor` (ex. 'red', '#FF0000', '#F00', 'FF0000', 'F00')
- [ ] add `isDateString` (ex. '12/12/2012', '12-12-2012', ...) and `isTimeString` (ex. '12:12', '12:12:12')
- [ ] add `isCamelCase`, `isCapitalized`, `isLowerCase`, `isSnakeCase` and `isUpperCase`
- [ ] add `isEqual`, `isEven` and `isOdd`
- [ ] add `isPositive` and `isNegative`
