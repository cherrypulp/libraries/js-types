import * as types from '../src/types';
import assert from 'assert';
import chai from 'chai';

const expect = chai.expect;
const should = chai.should;

const typesList = {
    arrayConstructor: new Array(),
    arrayLiteral: [],
    arrayLiteralPopulated: [0, 1, 2],
    booleanTrue: true,
    booleanFalse: false,
    'class': class {},
    date: new Date(),
    error: new Error(),
    'function': function () {},
    functionGenerator: function* () {},
    functionPromise: new Promise((resolve) => resolve()),
    objectConstructor: new Object(),
    objectLiteral: {},
    objectLiteralPopulated: {
        foo: 'bar',
    },
    nan: NaN,
    numberFloat: 1.4,
    numberInfinity: Infinity,
    numberInteger: 1,
    'null': null,
    regexConstructor: new RegExp('.*'),
    regexLiteral: /.*/,
    stringEmpty: '',
    stringPopulated: 'foo',
    symbol: Symbol(),
    'undefined': undefined,
};


describe('types.js', function() {
    describe('isArray', () => {
        it('return true if value is an array', () => {
            expect(types.isArray(typesList.arrayConstructor)).to.be.true;
            expect(types.isArray(typesList.arrayLiteral)).to.be.true;
            expect(types.isArray(typesList.arrayLiteralPopulated)).to.be.true;
        });
        it('return false if value is not an array', () => {
            expect(types.isArray(typesList.booleanFalse)).to.be.false;
            expect(types.isArray(typesList.booleanTrue)).to.be.false;
            expect(types.isArray(typesList.class)).to.be.false;
            expect(types.isArray(typesList.date)).to.be.false;
            expect(types.isArray(typesList.error)).to.be.false;
            expect(types.isArray(typesList.function)).to.be.false;
            expect(types.isArray(typesList.functionGenerator)).to.be.false;
            expect(types.isArray(typesList.functionPromise)).to.be.false;
            expect(types.isArray(typesList.objectConstructor)).to.be.false;
            expect(types.isArray(typesList.objectLiteralPopulated)).to.be.false;
            expect(types.isArray(typesList.nan)).to.be.false;
            expect(types.isArray(typesList.numberFloat)).to.be.false;
            expect(types.isArray(typesList.numberInfinity)).to.be.false;
            expect(types.isArray(typesList.numberInteger)).to.be.false;
            expect(types.isArray(typesList.null)).to.be.false;
            expect(types.isArray(typesList.regexConstructor)).to.be.false;
            expect(types.isArray(typesList.regexLiteral)).to.be.false;
            expect(types.isArray(typesList.stringEmpty)).to.be.false;
            expect(types.isArray(typesList.stringPopulated)).to.be.false;
            expect(types.isArray(typesList.symbol)).to.be.false;
            expect(types.isArray(typesList.undefined)).to.be.false;
        });
    });

    describe('isBoolean', () => {
        it('return true if value is a boolean', () => {
            expect(types.isBoolean(typesList.booleanFalse)).to.be.true;
            expect(types.isBoolean(typesList.booleanTrue)).to.be.true;
        });
        it('return false if value is not a boolean', () => {
            expect(types.isBoolean(typesList.arrayConstructor)).to.be.false;
            expect(types.isBoolean(typesList.arrayLiteral)).to.be.false;
            expect(types.isBoolean(typesList.arrayLiteralPopulated)).to.be.false;
            expect(types.isBoolean(typesList.class)).to.be.false;
            expect(types.isBoolean(typesList.date)).to.be.false;
            expect(types.isBoolean(typesList.error)).to.be.false;
            expect(types.isBoolean(typesList.function)).to.be.false;
            expect(types.isBoolean(typesList.functionGenerator)).to.be.false;
            expect(types.isBoolean(typesList.functionPromise)).to.be.false;
            expect(types.isBoolean(typesList.objectConstructor)).to.be.false;
            expect(types.isBoolean(typesList.objectLiteralPopulated)).to.be.false;
            expect(types.isBoolean(typesList.nan)).to.be.false;
            expect(types.isBoolean(typesList.numberFloat)).to.be.false;
            expect(types.isBoolean(typesList.numberInfinity)).to.be.false;
            expect(types.isBoolean(typesList.numberInteger)).to.be.false;
            expect(types.isBoolean(typesList.null)).to.be.false;
            expect(types.isBoolean(typesList.regexConstructor)).to.be.false;
            expect(types.isBoolean(typesList.regexLiteral)).to.be.false;
            expect(types.isBoolean(typesList.stringEmpty)).to.be.false;
            expect(types.isBoolean(typesList.stringPopulated)).to.be.false;
            expect(types.isBoolean(typesList.symbol)).to.be.false;
            expect(types.isBoolean(typesList.undefined)).to.be.false;
        });
    });

    describe('isCallable', () => {
        it('return true if value is a callable object', () => {
            expect(types.isCallable(typesList.class)).to.be.true;
            expect(types.isCallable(typesList.function)).to.be.true;
            expect(types.isCallable(typesList.functionGenerator)).to.be.true;
        });
        it('return false if value is not a callable object', () => {
            expect(types.isCallable(typesList.arrayConstructor)).to.be.false;
            expect(types.isCallable(typesList.arrayLiteral)).to.be.false;
            expect(types.isCallable(typesList.arrayLiteralPopulated)).to.be.false;
            expect(types.isCallable(typesList.booleanFalse)).to.be.false;
            expect(types.isCallable(typesList.booleanTrue)).to.be.false;
            expect(types.isCallable(typesList.date)).to.be.false;
            expect(types.isCallable(typesList.error)).to.be.false;
            expect(types.isCallable(typesList.functionPromise)).to.be.false;
            expect(types.isCallable(typesList.objectConstructor)).to.be.false;
            expect(types.isCallable(typesList.objectLiteralPopulated)).to.be.false;
            expect(types.isCallable(typesList.nan)).to.be.false;
            expect(types.isCallable(typesList.numberFloat)).to.be.false;
            expect(types.isCallable(typesList.numberInfinity)).to.be.false;
            expect(types.isCallable(typesList.numberInteger)).to.be.false;
            expect(types.isCallable(typesList.null)).to.be.false;
            expect(types.isCallable(typesList.regexConstructor)).to.be.false;
            expect(types.isCallable(typesList.regexLiteral)).to.be.false;
            expect(types.isCallable(typesList.stringEmpty)).to.be.false;
            expect(types.isCallable(typesList.stringPopulated)).to.be.false;
            expect(types.isCallable(typesList.symbol)).to.be.false;
            expect(types.isCallable(typesList.undefined)).to.be.false;
        });
    });

    describe('isDataURL', () => {
        it('return true if value is a string and data URL', () => {
            expect(types.isDataURL('data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7')).to.be.true;
            [
                "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQAQMAAAAlPW0iAAAABlBMVEUAAAD///+l2Z/dAAAAM0lEQVR4nGP4/5/h/1+G/58ZDrAz3D/McH8yw83NDDeNGe4Ug9C9zwz3gVLMDA/A6P9/AFGGFyjOXZtQAAAAAElFTkSuQmCC",
                "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAIBAMAAAA2IaO4AAAAFVBMVEXk5OTn5+ft7e319fX29vb5+fn///++GUmVAAAALUlEQVQIHWNICnYLZnALTgpmMGYIFWYIZTA2ZFAzTTFlSDFVMwVyQhmAwsYMAKDaBy0axX/iAAAAAElFTkSuQmCC",
                "   data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAIBAMAAAA2IaO4AAAAFVBMVEXk5OTn5+ft7e319fX29vb5+fn///++GUmVAAAALUlEQVQIHWNICnYLZnALTgpmMGYIFWYIZTA2ZFAzTTFlSDFVMwVyQhmAwsYMAKDaBy0axX/iAAAAAElFTkSuQmCC   ",
                " data:,Hello%2C%20World!",
                " data:,Hello World!",
                " data:text/plain;base64,SGVsbG8sIFdvcmxkIQ%3D%3D",
                " data:text/html,%3Ch1%3EHello%2C%20World!%3C%2Fh1%3E",
                "data:,A%20brief%20note",
                "data:text/html;charset=US-ASCII,%3Ch1%3EHello!%3C%2Fh1%3E"
            ].forEach((value) => expect(types.isDataURL(value)).to.be.true);
        });
        it('return false if value is not a data URL', () => {
            expect(types.isDataURL(typesList.arrayConstructor)).to.be.false;
            expect(types.isDataURL(typesList.arrayLiteral)).to.be.false;
            expect(types.isDataURL(typesList.arrayLiteralPopulated)).to.be.false;
            expect(types.isDataURL(typesList.booleanFalse)).to.be.false;
            expect(types.isDataURL(typesList.booleanTrue)).to.be.false;
            expect(types.isDataURL(typesList.class)).to.be.false;
            expect(types.isDataURL(typesList.date)).to.be.false;
            expect(types.isDataURL(typesList.error)).to.be.false;
            expect(types.isDataURL(typesList.function)).to.be.false;
            expect(types.isDataURL(typesList.functionGenerator)).to.be.false;
            expect(types.isDataURL(typesList.functionPromise)).to.be.false;
            expect(types.isDataURL(typesList.objectConstructor)).to.be.false;
            expect(types.isDataURL(typesList.objectLiteralPopulated)).to.be.false;
            expect(types.isDataURL(typesList.nan)).to.be.false;
            expect(types.isDataURL(typesList.numberFloat)).to.be.false;
            expect(types.isDataURL(typesList.numberInfinity)).to.be.false;
            expect(types.isDataURL(typesList.numberInteger)).to.be.false;
            expect(types.isDataURL(typesList.null)).to.be.false;
            expect(types.isDataURL(typesList.regexConstructor)).to.be.false;
            expect(types.isDataURL(typesList.regexLiteral)).to.be.false;
            expect(types.isDataURL(typesList.stringEmpty)).to.be.false;
            expect(types.isDataURL(typesList.stringPopulated)).to.be.false;
            expect(types.isDataURL(typesList.symbol)).to.be.false;
            expect(types.isDataURL(typesList.undefined)).to.be.false;
            [
                "dataxbase64",
                "data:HelloWorld",
                "data:text/html;charset=,%3Ch1%3EHello!%3C%2Fh1%3E",
                "data:text/html;charset,%3Ch1%3EHello!%3C%2Fh1%3E", "data:base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQAQMAAAAlPW0iAAAABlBMVEUAAAD///+l2Z/dAAAAM0lEQVR4nGP4/5/h/1+G/58ZDrAz3D/McH8yw83NDDeNGe4Ug9C9zwz3gVLMDA/A6P9/AFGGFyjOXZtQAAAAAElFTkSuQmCC",
                "",
                "http://wikipedia.org",
                "base64",
                "iVBORw0KGgoAAAANSUhEUgAAABAAAAAQAQMAAAAlPW0iAAAABlBMVEUAAAD///+l2Z/dAAAAM0lEQVR4nGP4/5/h/1+G/58ZDrAz3D/McH8yw83NDDeNGe4Ug9C9zwz3gVLMDA/A6P9/AFGGFyjOXZtQAAAAAElFTkSuQmCC"
            ].forEach((value) => expect(types.isDataURL(value)).to.be.false);
        });
    });

    describe('isDate', () => {
        it('return true if value is a Date object', () => {
            expect(types.isDate(typesList.date)).to.be.true;
        });
        it('return false if value is not a Date object', () => {
            expect(types.isDate(typesList.arrayConstructor)).to.be.false;
            expect(types.isDate(typesList.arrayLiteral)).to.be.false;
            expect(types.isDate(typesList.arrayLiteralPopulated)).to.be.false;
            expect(types.isDate(typesList.booleanFalse)).to.be.false;
            expect(types.isDate(typesList.booleanTrue)).to.be.false;
            expect(types.isDate(typesList.class)).to.be.false;
            expect(types.isDate(typesList.error)).to.be.false;
            expect(types.isDate(typesList.function)).to.be.false;
            expect(types.isDate(typesList.functionGenerator)).to.be.false;
            expect(types.isDate(typesList.functionPromise)).to.be.false;
            expect(types.isDate(typesList.objectConstructor)).to.be.false;
            expect(types.isDate(typesList.objectLiteralPopulated)).to.be.false;
            expect(types.isDate(typesList.nan)).to.be.false;
            expect(types.isDate(typesList.numberFloat)).to.be.false;
            expect(types.isDate(typesList.numberInfinity)).to.be.false;
            expect(types.isDate(typesList.numberInteger)).to.be.false;
            expect(types.isDate(typesList.null)).to.be.false;
            expect(types.isDate(typesList.regexConstructor)).to.be.false;
            expect(types.isDate(typesList.regexLiteral)).to.be.false;
            expect(types.isDate(typesList.stringEmpty)).to.be.false;
            expect(types.isDate(typesList.stringPopulated)).to.be.false;
            expect(types.isDate(typesList.symbol)).to.be.false;
            expect(types.isDate(typesList.undefined)).to.be.false;
        });
    });

    describe('isEmail', () => {
        it('return true if value is an email', () => {
            expect(types.isEmail('john@doe.com')).to.be.true;
            [
                'something@something.com',
                'someone@localhost.localdomain',
                'someone@127.0.0.1',
                'a@b.b',
                'a/b@domain.com',
                '{}@domain.com',
                'm*\'!%@something.sa',
                'tu!!7n7.ad##0!!!@company.ca',
                '%@com.com',
                //'!#$%&\'*+/=?^_`{|}~.-@com.com',
                // '.wooly@example.com',
                // 'wo..oly@example.com',
                'someone@do-ma-in.com',
                // 'somebody@example',
                'a@p.com',
            ].forEach((value) => expect(types.isEmail(value), value).to.be.true);
        });
        it('return false if value is not an email', () => {
            expect(types.isEmail(typesList.arrayConstructor)).to.be.false;
            expect(types.isEmail(typesList.arrayLiteral)).to.be.false;
            expect(types.isEmail(typesList.arrayLiteralPopulated)).to.be.false;
            expect(types.isEmail(typesList.booleanFalse)).to.be.false;
            expect(types.isEmail(typesList.booleanTrue)).to.be.false;
            expect(types.isEmail(typesList.class)).to.be.false;
            expect(types.isEmail(typesList.date)).to.be.false;
            expect(types.isEmail(typesList.error)).to.be.false;
            expect(types.isEmail(typesList.function)).to.be.false;
            expect(types.isEmail(typesList.functionGenerator)).to.be.false;
            expect(types.isEmail(typesList.functionPromise)).to.be.false;
            expect(types.isEmail(typesList.objectConstructor)).to.be.false;
            expect(types.isEmail(typesList.objectLiteralPopulated)).to.be.false;
            expect(types.isEmail(typesList.nan)).to.be.false;
            expect(types.isEmail(typesList.numberFloat)).to.be.false;
            expect(types.isEmail(typesList.numberInfinity)).to.be.false;
            expect(types.isEmail(typesList.numberInteger)).to.be.false;
            expect(types.isEmail(typesList.null)).to.be.false;
            expect(types.isEmail(typesList.regexConstructor)).to.be.false;
            expect(types.isEmail(typesList.regexLiteral)).to.be.false;
            expect(types.isEmail(typesList.stringEmpty)).to.be.false;
            expect(types.isEmail(typesList.stringPopulated)).to.be.false;
            expect(types.isEmail(typesList.symbol)).to.be.false;
            expect(types.isEmail(typesList.undefined)).to.be.false;
            [
                'invalid:email@example.com',
                '@somewhere.com',
                'example.com',
                '@@example.com',
                'a space@example.com',
                'something@ex..ample.com',
                'a\b@c',
                'someone@somewhere.com.',
                '""test\blah""@example.com"',
                // '"testblah"@example.com',
                'someone@somewhere.com@',
                'someone@somewhere_com',
                'someone@some:where.com',
                '.',
                'F/s/f/a@feo+re.com',
                'some+long+email+address@some+host-weird-/looking.com',
                'a @p.com',
                'a\u0020@p.com',
                'a\u0009@p.com',
                'a\u000B@p.com',
                'a\u000C@p.com',
                'a\u2003@p.com',
                'a\u3000@p.com',
                'ddjk-s-jk@asl-.com',
                'someone@do-.com',
                'somebody@-p.com',
                'somebody@-.com',
            ].forEach((value) => expect(types.isEmail(value), value).to.be.false);
        });
    });

    describe('isEmpty', () => {
        it('return true if value is empty', () => {
            expect(types.isEmpty(typesList.arrayConstructor)).to.be.true;
            expect(types.isEmpty(typesList.arrayLiteral)).to.be.true;
            expect(types.isEmpty(typesList.objectConstructor)).to.be.true;
            expect(types.isEmpty(typesList.null)).to.be.true;
            expect(types.isEmpty(typesList.stringEmpty)).to.be.true;
            expect(types.isEmpty(typesList.undefined)).to.be.true;
        });
        it('return false if value is not empty', () => {
            expect(types.isEmpty(typesList.arrayLiteralPopulated)).to.be.false;
            expect(types.isEmpty(typesList.booleanFalse)).to.be.false;
            expect(types.isEmpty(typesList.booleanTrue)).to.be.false;
            expect(types.isEmpty(typesList.class)).to.be.false;
            expect(types.isEmpty(typesList.date)).to.be.false;
            expect(types.isEmpty(typesList.error)).to.be.false;
            expect(types.isEmpty(typesList.function)).to.be.false;
            expect(types.isEmpty(typesList.functionGenerator)).to.be.false;
            expect(types.isEmpty(typesList.functionPromise)).to.be.false;
            expect(types.isEmpty(typesList.objectLiteralPopulated)).to.be.false;
            expect(types.isEmpty(typesList.nan)).to.be.false;
            expect(types.isEmpty(typesList.numberFloat)).to.be.false;
            expect(types.isEmpty(typesList.numberInfinity)).to.be.false;
            expect(types.isEmpty(typesList.numberInteger)).to.be.false;
            expect(types.isEmpty(typesList.regexConstructor)).to.be.false;
            expect(types.isEmpty(typesList.regexLiteral)).to.be.false;
            expect(types.isEmpty(typesList.stringPopulated)).to.be.false;
            expect(types.isEmpty(typesList.symbol)).to.be.false;
        });
    });

    describe('isError', () => {
        it('return true if value is an Error object', () => {
            expect(types.isError(typesList.error)).to.be.true;
        });
        it('return false if value is not an Error object', () => {
            expect(types.isError(typesList.arrayConstructor)).to.be.false;
            expect(types.isError(typesList.arrayLiteral)).to.be.false;
            expect(types.isError(typesList.arrayLiteralPopulated)).to.be.false;
            expect(types.isError(typesList.booleanFalse)).to.be.false;
            expect(types.isError(typesList.booleanTrue)).to.be.false;
            expect(types.isError(typesList.class)).to.be.false;
            expect(types.isError(typesList.date)).to.be.false;
            expect(types.isError(typesList.function)).to.be.false;
            expect(types.isError(typesList.functionGenerator)).to.be.false;
            expect(types.isError(typesList.functionPromise)).to.be.false;
            expect(types.isError(typesList.objectConstructor)).to.be.false;
            expect(types.isError(typesList.objectLiteralPopulated)).to.be.false;
            expect(types.isError(typesList.nan)).to.be.false;
            expect(types.isError(typesList.numberFloat)).to.be.false;
            expect(types.isError(typesList.numberInfinity)).to.be.false;
            expect(types.isError(typesList.numberInteger)).to.be.false;
            expect(types.isError(typesList.null)).to.be.false;
            expect(types.isError(typesList.regexConstructor)).to.be.false;
            expect(types.isError(typesList.regexLiteral)).to.be.false;
            expect(types.isError(typesList.stringEmpty)).to.be.false;
            expect(types.isError(typesList.stringPopulated)).to.be.false;
            expect(types.isError(typesList.symbol)).to.be.false;
            expect(types.isError(typesList.undefined)).to.be.false;
        });
    });

    describe('isFalse', () => {
        it('return true if value is a boolean and false', () => {
            expect(types.isFalse(typesList.booleanFalse)).to.be.true;
        });
        it('return false if value is not a boolean false', () => {
            expect(types.isFalse(typesList.arrayConstructor)).to.be.false;
            expect(types.isFalse(typesList.arrayLiteral)).to.be.false;
            expect(types.isFalse(typesList.arrayLiteralPopulated)).to.be.false;
            expect(types.isFalse(typesList.booleanTrue)).to.be.false;
            expect(types.isFalse(typesList.class)).to.be.false;
            expect(types.isFalse(typesList.date)).to.be.false;
            expect(types.isFalse(typesList.error)).to.be.false;
            expect(types.isFalse(typesList.function)).to.be.false;
            expect(types.isFalse(typesList.functionGenerator)).to.be.false;
            expect(types.isFalse(typesList.functionPromise)).to.be.false;
            expect(types.isFalse(typesList.objectConstructor)).to.be.false;
            expect(types.isFalse(typesList.objectLiteralPopulated)).to.be.false;
            expect(types.isFalse(typesList.nan)).to.be.false;
            expect(types.isFalse(typesList.numberFloat)).to.be.false;
            expect(types.isFalse(typesList.numberInfinity)).to.be.false;
            expect(types.isFalse(typesList.numberInteger)).to.be.false;
            expect(types.isFalse(typesList.null)).to.be.false;
            expect(types.isFalse(typesList.regexConstructor)).to.be.false;
            expect(types.isFalse(typesList.regexLiteral)).to.be.false;
            expect(types.isFalse(typesList.stringEmpty)).to.be.false;
            expect(types.isFalse(typesList.stringPopulated)).to.be.false;
            expect(types.isFalse(typesList.symbol)).to.be.false;
            expect(types.isFalse(typesList.undefined)).to.be.false;
        });
    });

    describe('isFloat', () => {
        it('return true if value is a float', () => {
            expect(types.isFloat(typesList.numberFloat)).to.be.true;
        });
        it('return false if value is not a float', () => {
            expect(types.isFloat(typesList.arrayConstructor)).to.be.false;
            expect(types.isFloat(typesList.arrayLiteral)).to.be.false;
            expect(types.isFloat(typesList.arrayLiteralPopulated)).to.be.false;
            expect(types.isFloat(typesList.booleanFalse)).to.be.false;
            expect(types.isFloat(typesList.booleanTrue)).to.be.false;
            expect(types.isFloat(typesList.class)).to.be.false;
            expect(types.isFloat(typesList.date)).to.be.false;
            expect(types.isFloat(typesList.error)).to.be.false;
            expect(types.isFloat(typesList.function)).to.be.false;
            expect(types.isFloat(typesList.functionGenerator)).to.be.false;
            expect(types.isFloat(typesList.functionPromise)).to.be.false;
            expect(types.isFloat(typesList.objectConstructor)).to.be.false;
            expect(types.isFloat(typesList.objectLiteralPopulated)).to.be.false;
            expect(types.isFloat(typesList.nan)).to.be.false;
            expect(types.isFloat(typesList.numberInfinity)).to.be.false;
            expect(types.isFloat(typesList.numberInteger)).to.be.false;
            expect(types.isFloat(typesList.null)).to.be.false;
            expect(types.isFloat(typesList.regexConstructor)).to.be.false;
            expect(types.isFloat(typesList.regexLiteral)).to.be.false;
            expect(types.isFloat(typesList.stringEmpty)).to.be.false;
            expect(types.isFloat(typesList.stringPopulated)).to.be.false;
            expect(types.isFloat(typesList.symbol)).to.be.false;
            expect(types.isFloat(typesList.undefined)).to.be.false;
        });
    });

    describe('isFunction', () => {
        it('return true if value is a function', () => {
            expect(types.isFunction(typesList.class)).to.be.true;
            expect(types.isFunction(typesList.function)).to.be.true;
        });
        it('return false if value is not a function', () => {
            expect(types.isFunction(typesList.arrayConstructor)).to.be.false;
            expect(types.isFunction(typesList.arrayLiteral)).to.be.false;
            expect(types.isFunction(typesList.arrayLiteralPopulated)).to.be.false;
            expect(types.isFunction(typesList.booleanFalse)).to.be.false;
            expect(types.isFunction(typesList.booleanTrue)).to.be.false;
            expect(types.isFunction(typesList.date)).to.be.false;
            expect(types.isFunction(typesList.error)).to.be.false;
            expect(types.isFunction(typesList.functionGenerator)).to.be.false;
            expect(types.isFunction(typesList.functionPromise)).to.be.false;
            expect(types.isFunction(typesList.objectConstructor)).to.be.false;
            expect(types.isFunction(typesList.objectLiteralPopulated)).to.be.false;
            expect(types.isFunction(typesList.nan)).to.be.false;
            expect(types.isFunction(typesList.numberFloat)).to.be.false;
            expect(types.isFunction(typesList.numberInfinity)).to.be.false;
            expect(types.isFunction(typesList.numberInteger)).to.be.false;
            expect(types.isFunction(typesList.null)).to.be.false;
            expect(types.isFunction(typesList.regexConstructor)).to.be.false;
            expect(types.isFunction(typesList.regexLiteral)).to.be.false;
            expect(types.isFunction(typesList.stringEmpty)).to.be.false;
            expect(types.isFunction(typesList.stringPopulated)).to.be.false;
            expect(types.isFunction(typesList.symbol)).to.be.false;
            expect(types.isFunction(typesList.undefined)).to.be.false;
        });
    });

    describe('isInteger', () => {
        it('return true if value is an integer', () => {
            expect(types.isInteger(typesList.numberInteger)).to.be.true;
        });
        it('return false if value is not an integer', () => {
            expect(types.isInteger(typesList.arrayConstructor)).to.be.false;
            expect(types.isInteger(typesList.arrayLiteral)).to.be.false;
            expect(types.isInteger(typesList.arrayLiteralPopulated)).to.be.false;
            expect(types.isInteger(typesList.booleanFalse)).to.be.false;
            expect(types.isInteger(typesList.booleanTrue)).to.be.false;
            expect(types.isInteger(typesList.class)).to.be.false;
            expect(types.isInteger(typesList.date)).to.be.false;
            expect(types.isInteger(typesList.error)).to.be.false;
            expect(types.isInteger(typesList.function)).to.be.false;
            expect(types.isInteger(typesList.functionGenerator)).to.be.false;
            expect(types.isInteger(typesList.functionPromise)).to.be.false;
            expect(types.isInteger(typesList.objectConstructor)).to.be.false;
            expect(types.isInteger(typesList.objectLiteralPopulated)).to.be.false;
            expect(types.isInteger(typesList.nan)).to.be.false;
            expect(types.isInteger(typesList.numberFloat)).to.be.false;
            expect(types.isInteger(typesList.numberInfinity)).to.be.false;
            expect(types.isInteger(typesList.null)).to.be.false;
            expect(types.isInteger(typesList.regexConstructor)).to.be.false;
            expect(types.isInteger(typesList.regexLiteral)).to.be.false;
            expect(types.isInteger(typesList.stringEmpty)).to.be.false;
            expect(types.isInteger(typesList.stringPopulated)).to.be.false;
            expect(types.isInteger(typesList.symbol)).to.be.false;
            expect(types.isInteger(typesList.undefined)).to.be.false;
        });
    });

    describe('isNull', () => {
        it('return true if value is null', () => {
            expect(types.isNull(typesList.null)).to.be.true;
        });
        it('return false if value is not null', () => {
            expect(types.isNull(typesList.arrayConstructor)).to.be.false;
            expect(types.isNull(typesList.arrayLiteral)).to.be.false;
            expect(types.isNull(typesList.arrayLiteralPopulated)).to.be.false;
            expect(types.isNull(typesList.booleanFalse)).to.be.false;
            expect(types.isNull(typesList.booleanTrue)).to.be.false;
            expect(types.isNull(typesList.class)).to.be.false;
            expect(types.isNull(typesList.date)).to.be.false;
            expect(types.isNull(typesList.error)).to.be.false;
            expect(types.isNull(typesList.function)).to.be.false;
            expect(types.isNull(typesList.functionGenerator)).to.be.false;
            expect(types.isNull(typesList.functionPromise)).to.be.false;
            expect(types.isNull(typesList.objectConstructor)).to.be.false;
            expect(types.isNull(typesList.objectLiteralPopulated)).to.be.false;
            expect(types.isNull(typesList.nan)).to.be.false;
            expect(types.isNull(typesList.numberFloat)).to.be.false;
            expect(types.isNull(typesList.numberInfinity)).to.be.false;
            expect(types.isNull(typesList.numberInteger)).to.be.false;
            expect(types.isNull(typesList.regexConstructor)).to.be.false;
            expect(types.isNull(typesList.regexLiteral)).to.be.false;
            expect(types.isNull(typesList.stringEmpty)).to.be.false;
            expect(types.isNull(typesList.stringPopulated)).to.be.false;
            expect(types.isNull(typesList.symbol)).to.be.false;
            expect(types.isNull(typesList.undefined)).to.be.false;
        });
    });

    describe('isNumeric', () => {
        it('return true if value is a number', () => {
            expect(types.isNumeric(typesList.numberFloat)).to.be.true;
            expect(types.isNumeric(typesList.numberInteger)).to.be.true;
        });
        it('return false if value is not a number', () => {
            expect(types.isNumeric(typesList.arrayConstructor)).to.be.false;
            expect(types.isNumeric(typesList.arrayLiteral)).to.be.false;
            expect(types.isNumeric(typesList.arrayLiteralPopulated)).to.be.false;
            expect(types.isNumeric(typesList.booleanFalse)).to.be.false;
            expect(types.isNumeric(typesList.booleanTrue)).to.be.false;
            expect(types.isNumeric(typesList.class)).to.be.false;
            expect(types.isNumeric(typesList.date)).to.be.false;
            expect(types.isNumeric(typesList.error)).to.be.false;
            expect(types.isNumeric(typesList.function)).to.be.false;
            expect(types.isNumeric(typesList.functionGenerator)).to.be.false;
            expect(types.isNumeric(typesList.functionPromise)).to.be.false;
            expect(types.isNumeric(typesList.numberInfinity)).to.be.false;
            expect(types.isNumeric(typesList.objectConstructor)).to.be.false;
            expect(types.isNumeric(typesList.objectLiteralPopulated)).to.be.false;
            expect(types.isNumeric(typesList.nan)).to.be.false;
            expect(types.isNumeric(typesList.null)).to.be.false;
            expect(types.isNumeric(typesList.regexConstructor)).to.be.false;
            expect(types.isNumeric(typesList.regexLiteral)).to.be.false;
            expect(types.isNumeric(typesList.stringEmpty)).to.be.false;
            expect(types.isNumeric(typesList.stringPopulated)).to.be.false;
            expect(types.isNumeric(typesList.symbol)).to.be.false;
            expect(types.isNumeric(typesList.undefined)).to.be.false;
        });
    });

    describe('isObject', () => {
        it('return true if value is an object', () => {
            expect(types.isObject(typesList.objectConstructor)).to.be.true;
            expect(types.isObject(typesList.objectLiteralPopulated)).to.be.true;
        });
        it('return false if value is not an object', () => {
            expect(types.isObject(typesList.arrayConstructor)).to.be.false;
            expect(types.isObject(typesList.arrayLiteral)).to.be.false;
            expect(types.isObject(typesList.arrayLiteralPopulated)).to.be.false;
            expect(types.isObject(typesList.booleanFalse)).to.be.false;
            expect(types.isObject(typesList.booleanTrue)).to.be.false;
            expect(types.isObject(typesList.class)).to.be.false;
            expect(types.isObject(typesList.date)).to.be.false;
            expect(types.isObject(typesList.error)).to.be.false;
            expect(types.isObject(typesList.function)).to.be.false;
            expect(types.isObject(typesList.functionGenerator)).to.be.false;
            expect(types.isObject(typesList.functionPromise)).to.be.false;
            expect(types.isObject(typesList.nan)).to.be.false;
            expect(types.isObject(typesList.numberFloat)).to.be.false;
            expect(types.isObject(typesList.numberInfinity)).to.be.false;
            expect(types.isObject(typesList.numberInteger)).to.be.false;
            expect(types.isObject(typesList.null)).to.be.false;
            expect(types.isObject(typesList.regexConstructor)).to.be.false;
            expect(types.isObject(typesList.regexLiteral)).to.be.false;
            expect(types.isObject(typesList.stringEmpty)).to.be.false;
            expect(types.isObject(typesList.stringPopulated)).to.be.false;
            expect(types.isObject(typesList.symbol)).to.be.false;
            expect(types.isObject(typesList.undefined)).to.be.false;
        });
    });

    describe('isPromise', () => {
        it('return true if value is a Promise', () => {
            expect(types.isPromise(typesList.functionPromise)).to.be.true;
        });
        it('return false if value is not a Promise', () => {
            expect(types.isPromise(typesList.arrayConstructor)).to.be.false;
            expect(types.isPromise(typesList.arrayLiteral)).to.be.false;
            expect(types.isPromise(typesList.arrayLiteralPopulated)).to.be.false;
            expect(types.isPromise(typesList.booleanFalse)).to.be.false;
            expect(types.isPromise(typesList.booleanTrue)).to.be.false;
            expect(types.isPromise(typesList.class)).to.be.false;
            expect(types.isPromise(typesList.date)).to.be.false;
            expect(types.isPromise(typesList.error)).to.be.false;
            expect(types.isPromise(typesList.function)).to.be.false;
            expect(types.isPromise(typesList.functionGenerator)).to.be.false;
            expect(types.isPromise(typesList.objectConstructor)).to.be.false;
            expect(types.isPromise(typesList.objectLiteralPopulated)).to.be.false;
            expect(types.isPromise(typesList.nan)).to.be.false;
            expect(types.isPromise(typesList.numberFloat)).to.be.false;
            expect(types.isPromise(typesList.numberInfinity)).to.be.false;
            expect(types.isPromise(typesList.numberInteger)).to.be.false;
            expect(types.isPromise(typesList.null)).to.be.false;
            expect(types.isPromise(typesList.regexConstructor)).to.be.false;
            expect(types.isPromise(typesList.regexLiteral)).to.be.false;
            expect(types.isPromise(typesList.stringEmpty)).to.be.false;
            expect(types.isPromise(typesList.stringPopulated)).to.be.false;
            expect(types.isPromise(typesList.symbol)).to.be.false;
            expect(types.isPromise(typesList.undefined)).to.be.false;
        });
    });

    describe('isRegExp', () => {
        it('return true if value is a RegExp', () => {
            expect(types.isRegExp(typesList.regexConstructor)).to.be.true;
            expect(types.isRegExp(typesList.regexLiteral)).to.be.true;
        });
        it('return false if value is not a RegExp', () => {
            expect(types.isRegExp(typesList.arrayConstructor)).to.be.false;
            expect(types.isRegExp(typesList.arrayLiteral)).to.be.false;
            expect(types.isRegExp(typesList.arrayLiteralPopulated)).to.be.false;
            expect(types.isRegExp(typesList.booleanFalse)).to.be.false;
            expect(types.isRegExp(typesList.booleanTrue)).to.be.false;
            expect(types.isRegExp(typesList.class)).to.be.false;
            expect(types.isRegExp(typesList.date)).to.be.false;
            expect(types.isRegExp(typesList.error)).to.be.false;
            expect(types.isRegExp(typesList.function)).to.be.false;
            expect(types.isRegExp(typesList.functionGenerator)).to.be.false;
            expect(types.isRegExp(typesList.functionPromise)).to.be.false;
            expect(types.isRegExp(typesList.objectConstructor)).to.be.false;
            expect(types.isRegExp(typesList.objectLiteralPopulated)).to.be.false;
            expect(types.isRegExp(typesList.nan)).to.be.false;
            expect(types.isRegExp(typesList.numberFloat)).to.be.false;
            expect(types.isRegExp(typesList.numberInfinity)).to.be.false;
            expect(types.isRegExp(typesList.numberInteger)).to.be.false;
            expect(types.isRegExp(typesList.null)).to.be.false;
            expect(types.isRegExp(typesList.stringEmpty)).to.be.false;
            expect(types.isRegExp(typesList.stringPopulated)).to.be.false;
            expect(types.isRegExp(typesList.symbol)).to.be.false;
            expect(types.isRegExp(typesList.undefined)).to.be.false;
        });
    });

    describe('isString', () => {
        it('return true if value is a string', () => {
            expect(types.isString(typesList.stringEmpty)).to.be.true;
            expect(types.isString(typesList.stringPopulated)).to.be.true;
        });
        it('return false if value is not a string', () => {
            expect(types.isString(typesList.arrayConstructor)).to.be.false;
            expect(types.isString(typesList.arrayLiteral)).to.be.false;
            expect(types.isString(typesList.arrayLiteralPopulated)).to.be.false;
            expect(types.isString(typesList.booleanFalse)).to.be.false;
            expect(types.isString(typesList.booleanTrue)).to.be.false;
            expect(types.isString(typesList.class)).to.be.false;
            expect(types.isString(typesList.date)).to.be.false;
            expect(types.isString(typesList.error)).to.be.false;
            expect(types.isString(typesList.function)).to.be.false;
            expect(types.isString(typesList.functionGenerator)).to.be.false;
            expect(types.isString(typesList.functionPromise)).to.be.false;
            expect(types.isString(typesList.objectConstructor)).to.be.false;
            expect(types.isString(typesList.objectLiteralPopulated)).to.be.false;
            expect(types.isString(typesList.nan)).to.be.false;
            expect(types.isString(typesList.numberFloat)).to.be.false;
            expect(types.isString(typesList.numberInfinity)).to.be.false;
            expect(types.isString(typesList.numberInteger)).to.be.false;
            expect(types.isString(typesList.null)).to.be.false;
            expect(types.isString(typesList.regexConstructor)).to.be.false;
            expect(types.isString(typesList.regexLiteral)).to.be.false;
            expect(types.isString(typesList.symbol)).to.be.false;
            expect(types.isString(typesList.undefined)).to.be.false;
        });
    });

    describe('isSymbol', () => {
        it('return true if value is a symbol', () => {
            expect(types.isSymbol(typesList.symbol)).to.be.true;
        });
        it('return false if value is not a symbol', () => {
            expect(types.isSymbol(typesList.arrayConstructor)).to.be.false;
            expect(types.isSymbol(typesList.arrayLiteral)).to.be.false;
            expect(types.isSymbol(typesList.arrayLiteralPopulated)).to.be.false;
            expect(types.isSymbol(typesList.booleanFalse)).to.be.false;
            expect(types.isSymbol(typesList.booleanTrue)).to.be.false;
            expect(types.isSymbol(typesList.class)).to.be.false;
            expect(types.isSymbol(typesList.date)).to.be.false;
            expect(types.isSymbol(typesList.error)).to.be.false;
            expect(types.isSymbol(typesList.function)).to.be.false;
            expect(types.isSymbol(typesList.functionGenerator)).to.be.false;
            expect(types.isSymbol(typesList.functionPromise)).to.be.false;
            expect(types.isSymbol(typesList.objectConstructor)).to.be.false;
            expect(types.isSymbol(typesList.objectLiteralPopulated)).to.be.false;
            expect(types.isSymbol(typesList.nan)).to.be.false;
            expect(types.isSymbol(typesList.numberFloat)).to.be.false;
            expect(types.isSymbol(typesList.numberInfinity)).to.be.false;
            expect(types.isSymbol(typesList.numberInteger)).to.be.false;
            expect(types.isSymbol(typesList.null)).to.be.false;
            expect(types.isSymbol(typesList.regexConstructor)).to.be.false;
            expect(types.isSymbol(typesList.regexLiteral)).to.be.false;
            expect(types.isSymbol(typesList.stringEmpty)).to.be.false;
            expect(types.isSymbol(typesList.stringPopulated)).to.be.false;
            expect(types.isSymbol(typesList.undefined)).to.be.false;
        });
    });

    describe('isTrue', () => {
        it('return true if value is a boolean and true', () => {
            expect(types.isTrue(typesList.booleanTrue)).to.be.true;
        });
        it('return false if value is not a boolean and true', () => {
            expect(types.isTrue(typesList.arrayConstructor)).to.be.false;
            expect(types.isTrue(typesList.arrayLiteral)).to.be.false;
            expect(types.isTrue(typesList.arrayLiteralPopulated)).to.be.false;
            expect(types.isTrue(typesList.booleanFalse)).to.be.false;
            expect(types.isTrue(typesList.class)).to.be.false;
            expect(types.isTrue(typesList.date)).to.be.false;
            expect(types.isTrue(typesList.error)).to.be.false;
            expect(types.isTrue(typesList.function)).to.be.false;
            expect(types.isTrue(typesList.functionGenerator)).to.be.false;
            expect(types.isTrue(typesList.functionPromise)).to.be.false;
            expect(types.isTrue(typesList.objectConstructor)).to.be.false;
            expect(types.isTrue(typesList.objectLiteralPopulated)).to.be.false;
            expect(types.isTrue(typesList.nan)).to.be.false;
            expect(types.isTrue(typesList.numberFloat)).to.be.false;
            expect(types.isTrue(typesList.numberInfinity)).to.be.false;
            expect(types.isTrue(typesList.numberInteger)).to.be.false;
            expect(types.isTrue(typesList.null)).to.be.false;
            expect(types.isTrue(typesList.regexConstructor)).to.be.false;
            expect(types.isTrue(typesList.regexLiteral)).to.be.false;
            expect(types.isTrue(typesList.stringEmpty)).to.be.false;
            expect(types.isTrue(typesList.stringPopulated)).to.be.false;
            expect(types.isTrue(typesList.symbol)).to.be.false;
            expect(types.isTrue(typesList.undefined)).to.be.false;
        });
    });

    describe('isUndefined', () => {
        it('return true if value is undefined', () => {
            expect(types.isUndefined(typesList.undefined)).to.be.true;
        });
        it('return false if value is not undefined', () => {
            expect(types.isUndefined(typesList.arrayConstructor)).to.be.false;
            expect(types.isUndefined(typesList.arrayLiteral)).to.be.false;
            expect(types.isUndefined(typesList.arrayLiteralPopulated)).to.be.false;
            expect(types.isUndefined(typesList.booleanFalse)).to.be.false;
            expect(types.isUndefined(typesList.booleanTrue)).to.be.false;
            expect(types.isUndefined(typesList.class)).to.be.false;
            expect(types.isUndefined(typesList.date)).to.be.false;
            expect(types.isUndefined(typesList.error)).to.be.false;
            expect(types.isUndefined(typesList.function)).to.be.false;
            expect(types.isUndefined(typesList.functionGenerator)).to.be.false;
            expect(types.isUndefined(typesList.functionPromise)).to.be.false;
            expect(types.isUndefined(typesList.objectConstructor)).to.be.false;
            expect(types.isUndefined(typesList.objectLiteralPopulated)).to.be.false;
            expect(types.isUndefined(typesList.nan)).to.be.false;
            expect(types.isUndefined(typesList.numberFloat)).to.be.false;
            expect(types.isUndefined(typesList.numberInfinity)).to.be.false;
            expect(types.isUndefined(typesList.numberInteger)).to.be.false;
            expect(types.isUndefined(typesList.null)).to.be.false;
            expect(types.isUndefined(typesList.regexConstructor)).to.be.false;
            expect(types.isUndefined(typesList.regexLiteral)).to.be.false;
            expect(types.isUndefined(typesList.stringEmpty)).to.be.false;
            expect(types.isUndefined(typesList.stringPopulated)).to.be.false;
            expect(types.isUndefined(typesList.symbol)).to.be.false;
        });
    });
});
