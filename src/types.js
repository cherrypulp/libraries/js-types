import {DATA_URL_REGEXP, EMAIL_REGEXP, EMPTY_STRING_REGEXP} from './helpers';

/**
 * Return true if value is an array.
 * @param {*} value
 * @return {boolean}
 */
export function isArray(value) {
    return Array.isArray(value);
}

/**
 * Return true if value is a boolean.
 * @param {*} value
 * @return {boolean}
 */
export function isBoolean(value) {
    return isTypeOf(value, 'boolean');
}

/**
 * Return true if value is a callable object or function.
 * @param {*} value
 * @return {boolean}
 */
export function isCallable(value) {
    if (!value || isTypeOf(value, 'number') || isUndefined(value) || isUndefined(value.call)) {
        return false;
    }

    return true;
}

/**
 * Return true if value is date object.
 * @param {*} value
 * @return {boolean}
 */
export function isDate(value) {
    return value instanceof Date;
}

/**
 * Return true if value is a data URL.
 * @param {*} value
 * @return {boolean}
 */
export function isDataURL(value) {
    return isString(value) && DATA_URL_REGEXP.test(value);
}

/**
 * Return true if value is an email.
 * @param {*} value
 * @return {boolean}
 */
export function isEmail(value) {
    return isString(value) && EMAIL_REGEXP.test(value);
}

/**
 * Return true if value is empty ('', {}, [], null or undefined).
 * @param {*} value
 * @return {boolean}
 */
export function isEmpty(value) {
    if (isNull(value)) {
        return true;
    }

    if (isString(value)) {
        return EMPTY_STRING_REGEXP.test(value);
    }

    if (isArray(value)) {
        return value.length === 0;
    }

    if (isDate(value)) {
        return false;
    }

    if (isObject(value)) {
        return Object.values(value).length === 0;
    }

    return isUndefined(value);
}

/**
 * Return true if value is an Error object.
 * @param {*} value
 * @return {boolean}
 */
export function isError(value) {
    return value instanceof Error && !isUndefined(value.message);
}

/**
 * Return true if value is false.
 * @param {*} value
 * @return {boolean}
 */
export function isFalse(value) {
    return value === false;
}

/**
 * Return true if value is a float number.
 * @param {*} value
 * @return {boolean}
 */
export function isFloat(value) {
    return isTypeOf(value, 'number') && Boolean(value % 1);
}

/**
 * Return true if value is a function.
 * @param {*} value
 * @return {boolean}
 */
export function isFunction(value) {
    return isTypeOf(value, 'function');
}

/**
 * Return true if value is integer.
 * @param {*} value
 * @return {boolean}
 */
export function isInteger(value) {
    return isTypeOf(value, 'number') && Number.isFinite(value) && Math.floor(value) === value;
}

/**
 * Return true if value is null.
 * @param {*} value
 * @return {boolean}
 */
export function isNull(value) {
    return value === null;
}

/**
 * Return true if value is numeric.
 * @param {*} value
 * @return {boolean}
 */
export function isNumeric(value) {
    return isFloat(value) || isInteger(value);
}

/**
 * Return true if value is an object.
 * @param {*} value
 * @return {boolean}
 */
export function isObject(value) {
    return Boolean(value) && isTypeOf(value, 'object') && value.constructor === Object;
}

/**
 * Return true if value is a promise.
 * @param {*} value
 * @return {boolean}
 */
export function isPromise(value) {
    return (
        !isUndefined(value) && value instanceof Promise
        && isFunction(value.then)
        && isFunction(value.catch)
    );
}

/**
 * Return true if value is a RegExp.
 * @param {*} value
 * @return {boolean}
 */
export function isRegExp(value) {
    return Boolean(value) && isTypeOf(value, 'regexp') && value.constructor === RegExp;
}

/**
 * Return true if value is a string.
 * @param {*} value
 * @return {boolean}
 */
export function isString(value) {
    return isTypeOf(value, 'string') || value instanceof String;
}

/**
 * Return true if value is a symbol.
 * @param {*} value
 * @return {boolean}
 */
export function isSymbol(value) {
    return isTypeOf(value, 'symbol');
}

/**
 * Return true if value is true.
 * @param {*} value
 * @return {boolean}
 */
export function isTrue(value) {
    return value === true;
}

/**
 * Return true if value typeof is equal to given type.
 * @param {*} value
 * @param {string} type
 * @return {boolean}
 */
export function isTypeOf(value, type) {
    return Object.prototype.toString.call(value).slice(8, -1).toLowerCase() === type;
}

/**
 * Return true if value is undefined.
 * @param {*} value
 * @return {boolean}
 */
export function isUndefined(value) {
    return isTypeOf(value, 'undefined');
}
