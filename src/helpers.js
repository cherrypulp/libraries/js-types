/**
 * @see https://developer.mozilla.org/en-US/docs/data_URIs
 * @type {RegExp}
 */
export const DATA_URL_REGEXP = /^\s*data:([a-z]+\/[a-z]+(;[a-z-]+=[a-z-]+)?)?(;base64)?,[a-z0-9!$&',()*+,;=\-._~:@/?%\s]*\s*$/i;

/**
 * @see https://stackoverflow.com/a/201378/1420009
 * @type {RegExp}
 */
export const EMAIL_REGEXP = /^(?:[a-z0-9!#$%&'*/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*/=?^_`{|}~-]+)*|"(?:[\x01-x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-x09\x0b\x0c\x0e-\x7f]))\])$/i;

/**
 * @type {RegExp}
 */
export const EMPTY_STRING_REGEXP = /^\s*$/;
